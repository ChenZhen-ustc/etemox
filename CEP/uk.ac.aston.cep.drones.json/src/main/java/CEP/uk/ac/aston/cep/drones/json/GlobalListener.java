package CEP.uk.ac.aston.cep.drones.json;
import com.espertech.esper.runtime.client.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.espertech.esper.common.client.EventBean;
import com.espertech.esper.common.internal.event.json.core.JsonEventBean;

public class GlobalListener implements UpdateListener  {
	private final static Logger logger = LoggerFactory.getLogger(GlobalListener.class);
	public long startMillis1 = System.currentTimeMillis();
	static String pattern ="";
	static String detectedCE; 
	static MqttClient mqttClient;
	static MemoryPersistence persistence = new MemoryPersistence();
	public static String topic = "";
    static int qos             = 0; 
    static String broker       = "tcp://localhost:1883";
    static String clientId     = "CEPoutput";
  
	public GlobalListener(String name) throws MqttException {
		pattern = name;
		mqttClient = new MqttClient(broker, clientId, persistence);
	    MqttConnectOptions connOpts = new MqttConnectOptions();
	    connOpts.setCleanSession(false);
	    System.out.println("Connecting listener to broker: "+broker);
        mqttClient.connect(connOpts);
        System.out.println("Connected");
	}

	public void update(EventBean[] newEvents, EventBean[] oldEvents, EPStatement statement, EPRuntime runtime) {
		if (newEvents != null) {
			@SuppressWarnings("unchecked")
			
			HashMap<String, Object> payload = (HashMap<String, Object>) newEvents[0].getUnderlying();
			if (payload.keySet().contains("iteration")) {
				try {
					detectedCE = new ObjectMapper().writeValueAsString(payload);
				} catch (JsonProcessingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				topic= "stopper";
				mqttpublish(topic, ""+detectedCE);
				//System.out.println(detectedCE);
			}
		
			 for (HashMap.Entry<String, Object> entry : payload.entrySet()) {
                String key = entry.getKey();  
                Object value = entry.getValue();
                JsonEventBean bean = (JsonEventBean) value;     
                detectedCE= bean.getUnderlying().toString();
                if (key.equals("simpleEvent")) {
                	topic= "Q_table_collection_10.json";
                }else if (key.equals("simpleEvent100")) {
                	topic="Q_table_collection_100.json";
                }else if (key.equals("simpleEvent500")) {
                	topic="Q_table_collection_500.json";
                }else if (key.equals("LogExploration")) {
                	topic="Q_table_collection_exploration.json";
                } 
                mqttpublish(topic, detectedCE); 
               // System.out.println(bean.getUnderlying().toString());
            }
          
		}	
		
	}
	public void mqttpublish(String topic,String content) {
		MqttMessage messagePub = new MqttMessage(content.getBytes());
		messagePub.setQos(qos);
		try {
			mqttClient.publish(topic, messagePub);
		} catch (MqttPersistenceException e) {
			e.printStackTrace();
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

}
