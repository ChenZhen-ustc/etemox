package CEP.uk.ac.aston.cep.drones.json;
import com.espertech.esper.runtime.client.*;

import java.util.HashMap;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.espertech.esper.common.client.EventBean;
import com.espertech.esper.common.internal.event.json.core.JsonEventBean;

public class ExplorationListener implements UpdateListener  {
	private final static Logger logger = LoggerFactory.getLogger(ExplorationListener.class);
	public long startMillis1 = System.currentTimeMillis();
	static String pattern ="";
	static String detectedCE; 
	static MqttClient mqttClient;
	static MemoryPersistence persistence = new MemoryPersistence();
	public static String topic = "Q_table_collection_exploration.json";
    static int qos             = 2; 
    static String broker       = "tcp://localhost:1883";
    static String clientId     = "CEPoutput";
  
	public ExplorationListener(String name) throws MqttException {
		pattern = name;
		//mqttClient = new MqttClient(broker, clientId, persistence);
	    //MqttConnectOptions connOpts = new MqttConnectOptions();
	    //connOpts.setCleanSession(false);
	    //System.out.println("Connecting listener to broker: "+broker);
       // mqttClient.connect(connOpts);
       // System.out.println("Connected");
	}

	public void update(EventBean[] newEvents, EventBean[] oldEvents, EPStatement statement, EPRuntime runtime) {
		if (newEvents != null) {
			@SuppressWarnings("unchecked")
			
			HashMap<String, Object> payload = (HashMap<String, Object>) newEvents[0].getUnderlying();
			System.out.println(payload);
			 for (HashMap.Entry<String, Object> entry : payload.entrySet()) {
                String key = entry.getKey();
                //System.out.println(key);
                Object value = entry.getValue();
                if(key.equals("CounterExplora2")) {
                	System.out.println("Explora2:"+value.toString());
                }
                JsonEventBean bean = (JsonEventBean) value;     
                detectedCE= bean.getUnderlying().toString();
               // System.out.println(detectedCE);
                //mqttpublish(topic, detectedCE);
            }
          
		}	
		
	}
	public void mqttpublish(String topic,String content) {
		MqttMessage messagePub = new MqttMessage(content.getBytes());
		messagePub.setQos(qos);
		try {
			mqttClient.publish(topic, messagePub);
		} catch (MqttPersistenceException e) {
			e.printStackTrace();
		} catch (MqttException e) {
			e.printStackTrace();
		}
	}

}
