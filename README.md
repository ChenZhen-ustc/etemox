# ETeMoX

**ETeMoX** is an architecture based in Temporal Models and Complex Event Processing to enable History-aware Explanations in *goal-oriented systems*. 

![](images/etemox.png)

The core components of the architecture are:
- Translator
- Filter
- Temporal Model
- Explainer

To test the implementation, a user requires: i) an RL system that exposes its decision-making traces, ii) a parser to translate these traces into the [trace-metamodel](https://dl.acm.org/doi/abs/10.1145/3419804.3420276), iii) a set of event patterns that define the filtering criteria (and their deployment to the Esper CEP engine), iv) a Hawk instance indexing the translated traces into a temporal model, and v) a set of temporal queries that extract the history-aware explanations from the temporal model.

### Architecture requirements
- [Java 11](https://www.java.com/en/download/manual.jsp).
- [Eclipse Modeling Tools](https://www.eclipse.org/downloads/packages/release/2022-03/r/eclipse-modeling-tools), in a recent release.
- [Eclipse Hawk](https://www.eclipse.org/hawk/) as the model indexing tool.
- Complex Event Processing Engine [Esper](https://www.espertech.com/) (Esper 8.3 provided). 
- MQTT message broker [Mosquito](https://mosquitto.org/) (If a local MQTT broker is used).
- MQTT client library [Eclipse Paho](https://www.eclipse.org/paho/)
- Dependency manager [Ivy](https://ant.apache.org/ivy/) 

### Step-by-step
1. Clone the present repository in the command line.
```bash
	git clone [repo]
```
1. Open Eclipse IDE.
1. Import the Eclipse projects into the repository.
 `File -> Import -> Projects from Folder or Archive -> Directory -> ../path/to/etemoxFolder ->  Select All`
1. Install IvyDE 2.5.0. The update site URL for IvyDE can be found [here](https://ant.apache.org/ivy/ivyde/download.html).
1. Install Paho-mqtt 3.1 mqtt java client. Guidelines to install Paho-mqtt can be found [here](https://www.eclipse.org/paho/index.php?page=clients/java/index.php).
1. Install Xcore, by using "Help - Install New Software...", selecting the update site for your Eclipse release, and searching for "Xcore".
1. (Optional if a local mqtt broker is used) Install and run a Mosquitto broker. Guidelines can be found [here](https://mosquitto.org/download/).
1. Open the `.xcore` file in `STORMLog.xcore`: add and remove a comment to ensure the relevant code is generated in the project.
1. Open the target platform definition in `uk.ac.aston.rdm.targetplaform`, let it resolve, and set it as your target platform.
1. Retrieve external libraries for `uk.ac.aston.hawk.mqtt` and `uk.ac.aston.hawk.mongodb` through IvyDE: right click on each project in the Package Explorer, and select "Ivy - Retrieve 'dependencies'".
1. Right-click on the `uk.ac.aston.logmodel`  project in the Package Explorer, and select "Run As - Eclipse Application". This corresponds to the Temporal Model component of our architecture.
1. In this new Eclipse instance. Create New Hawk Instance "InstanceName". Check the guidelines for ccreating new Hawk index [here](https://www.eclipse.org/hawk/basic-use/core-concepts/):
	- InstanceType: Time-aware local Hawk
	- Updater: Default Hawk Time Aware Model Updater
	- Min/Max Delay: 0 and 0
![](images/hawkInstance.png)
	- (Advanced) Metamodel parsers: EMF Metamodel Resource Factory
	- (Advanced) Model parsers: Drone resource factory 
		> Note: This will vary depending on the case study.
		
	- (Advanced) Query engines: EOL Query Engine and Time Aware EOL query Engine.
    	
    	
![](images/advancedHawk.png)

12. Configure Indexer ("InstanceName"):
	- Metamodels: stormlog.xcore
	![](images/metamodelDefi.png)
	- (Indexed Locations) Type: MQTT Connector 
	- (Indexed Locations) Location: MQTT topics to monitor (e.g., tcp://localhost:1883/initial_setting.json?extra=Q_table_collection_10.json)
 		> Note: This will vary depending on the broker and the MQTT port used, `localhost:1883` for a mosquitto local broker running on 1883 port.
    
    ![](images/addRepoMqtt.png)
13. In the principal Eclipse instance, Run As Java Application `uk.ac.aston.cep.drones.json` (Right click-> Run As). This will initiate the CEP engine with the Filter criteria of Sampling every 10 steps.  
14. Run the sample case study Q-Learning. Check documentation [here](https://gitlab.com/sea-aston/etemox/-/tree/origin/2-etemox-first-commit/Q-Learning). In the terminal run: 
```python 
python main.py
```
15. In the Temporal Model (Eclipse `uk.ac.aston.logmodel`) instance, import the queries project.
`File -> Import -> Projects from Folder or Archive -> Directory -> ../path/to/etemoxFolder/hawk/EOLQueries ->  Select All`
16. Test any of the temporal queries from `uk.ac.aston.drones.queries`. Check [Hawk documentation](https://www.eclipse.org/hawk/basic-use/examples-xmi/) for examples.
 ![](images/queryResult.png)
	 	
	
	
	


